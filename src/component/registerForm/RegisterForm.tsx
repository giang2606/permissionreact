import React from "react";
import { Form, Input, Tooltip, Icon, Button, Select, message } from "antd";
import { graphql } from "react-apollo";
import flowright from "lodash.flowright";
import { createUserMutation } from "../../queries/queries";
const { Option } = Select;
export interface Props {
  form: any;
  createUserMutation: any;
}
export interface State {}
class RegistrationForm extends React.Component<Props, State> {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
        // console.log("Received values of form: ", values);
        this.props
          .createUserMutation({
            variables: {
              email: values.email,
              password: values.password,
              fullName: values.nickname,
              role: values.role
            }
          })
          .then((res: any) => {
            message.success("Đã tạo tài khoản thành công!");
          })
          .catch((err: any) => {
            message.error("Tài khoản đã tồn tại!");
          });
      } else {
        console.log(err);
      }
    });
  };

  handleConfirmBlur = (e: any) => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule: any, value: any, callback: any) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule: any, value: any, callback: any) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  handleWebsiteChange = (value: any) => {
    let autoCompleteResult: any;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = [".com", ".org", ".net"].map(
        domain => `${value}${domain}`
      );
    }
    this.setState({ autoCompleteResult });
  };
  reigisterSubmit = () => {};
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };
    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item label="E-mail">
          {getFieldDecorator("email", {
            rules: [
              {
                type: "email",
                message: "Email phải có dạng ...@xxx.com"
              },
              {
                required: true,
                message: "Bạn chưa điền Email!"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Mật Khẩu" hasFeedback>
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Bạn chưa nhập Mật Khẩu!"
              },
              {
                validator: this.validateToNextPassword
              }
            ]
          })(<Input.Password />)}
        </Form.Item>
        <Form.Item label="Nhập Lại Mật Khẩu" hasFeedback>
          {getFieldDecorator("confirm", {
            rules: [
              {
                required: true,
                message: "Mật khẩu không khớp!"
              },
              {
                validator: this.compareToFirstPassword
              }
            ]
          })(<Input.Password onBlur={this.handleConfirmBlur} />)}
        </Form.Item>
        <Form.Item
          label={
            <span>
              Tên Hiển Thị&nbsp;
              <Tooltip title="Tên này là tên mà chúng tôi gọi bạn">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          }
        >
          {getFieldDecorator("nickname", {
            rules: [
              {
                required: true,
                message: "Bạn chưa điền tên đăng nhập",
                whitespace: true
              }
            ]
          })(<Input />)}
        </Form.Item>

        <Form.Item label="Chọn Quyền">
          {getFieldDecorator("role")(
            <Select defaultValue="1">
              <Option value="1">Super Admin</Option>
              <Option value="2">Admin</Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button className="btn-reg " type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
const WrappedRegistrationForm = Form.create({ name: "register" })(
  RegistrationForm
);
export default flowright(
  graphql(createUserMutation, { name: "createUserMutation" })
)(WrappedRegistrationForm);
