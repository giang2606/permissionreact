import {
  Layout,
  Menu,
  Icon,
  Modal,
  Form,
  Input,
  Button,
  message,
  Upload,
  Popconfirm
} from "antd";
import * as React from "react";
import { Fragment } from "react";
import { getProductQuery } from "../../queries/queries";
import WrappedRegistrationForm from "../registerForm/RegisterForm";
const { Sider } = Layout;
const { SubMenu } = Menu;
const { TextArea } = Input;

export interface Props {
  creatProductMutation: any;
  loginQuery: any;
  history: any;
}
export interface State {
  collapsed: boolean;
  visible: boolean;
  confirmLoading: boolean;
  input: {
    quantity: string;
    title: string;
    content: string;
    price: string;
  };
  imageUrl: any;
  loading: boolean;
  loginVisible: boolean;
  email: string;
  password: string;
  visibleRegister: boolean;
}
export interface Function {
  collapsed: any;
  onCollapse(): void;
}
let getBase64 = (img: any, callback: any) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};
let beforeUpload = (file: any) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
};
class Side extends React.Component<Props, State> {
  id: any = localStorage.getItem("key");
  idUser: any = JSON.parse(this.id);
  constructor(props: any) {
    super(props);
    this.state = {
      collapsed: false,
      visible: false,
      confirmLoading: false,
      input: {
        quantity: "",
        title: "",
        content: "",
        price: ""
      },
      password: "",
      email: "",
      imageUrl: "",
      loading: false,
      loginVisible: false,
      visibleRegister: false
    };
  }
  handleChange = (info: any) => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        this.setState({
          imageUrl: imageUrl,
          loading: false
        })
      );
    }
  };
  handleCancleLogin = () => {
    this.setState({
      loginVisible: false
    });
  };
  onCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  modalLogin = () => {
    if (this.state.loginVisible === true) {
      return (
        <Modal
          footer={[
            <Button htmlType="submit" onClick={this.userLogin}>
              Đăng Nhập
            </Button>,
            <Button onClick={this.handleCancleLogin}>Đóng</Button>
          ]}
          onCancel={this.handleCancleLogin}
          visible={this.state.loginVisible}
          title="Đăng Nhập"
        >
          <Form>
            <Form.Item>
              <Input
                onChange={event => this.getInputLogin(event)}
                prefix={
                  <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                name="email"
                placeholder="Email"
              />
            </Form.Item>
            <Form.Item>
              <Input
                onChange={event => this.getInputLogin(event)}
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
                name="password"
              />
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  };
  handleCancel = () => {
    this.setState({
      visible: false
    });
  };
  createProduct = () => {
    let id: any = localStorage.getItem("key");
    let idUser: any = JSON.parse(id);
    this.props.creatProductMutation({
      variables: {
        quantity: this.state.input.quantity,
        title: this.state.input.title,
        content: this.state.input.content,
        price: this.state.input.price,
        img: this.state.imageUrl,
        UserID: idUser.login.id,
        userName: idUser.login.fullName
      },
      refetchQueries: [{ query: getProductQuery }]
    });
  };
  userLogin = () => {
    this.props
      .loginQuery({
        variables: {
          email: this.state.email,
          password: this.state.password
        }
      })
      .then((res: any) => {
        localStorage.setItem("key", JSON.stringify(res.data));
        this.setState({
          loginVisible: false
        });
        window.location.reload();
      })
      .catch((err: any) => {
        message.error("Email hoặc Password không đúng!");
      });
  };
  console.log('sadad');
  
  submitForm = (event: any) => {
    event.preventDefault();
    if (localStorage.getItem("key")) {
      this.createProduct();
      message.success("Tạo thành công");
    } else {
      this.checkLogined();
    }
  };
  showLoginForm = () => {
    this.setState({
      loginVisible: true
    });
  };
  checkLogined = () => {
    message.warning("Vui lòng đăng nhập để sử dụng chức năng này!");
    this.showLoginForm();
    this.setState({
      visible: false
    });
  };
  isChanged = (event: any) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      input: { ...this.state.input, [name]: value }
    });
  };
  getInputLogin = (event: any) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      ...this.state,
      [name]: value
    });
  };
  logoutFunction = () => {
    if (localStorage.getItem("key")) {
      localStorage.removeItem("key");
      window.location.reload();
    } else {
      this.checkLogined();
    }
  };
  showRegisterForm = () => {
    this.setState({
      visibleRegister: true
    });
  };
  closeRegister = (): any => {
    this.setState({
      visibleRegister: false
    });
  };
  renderRegisterForm = () => {
    return (
      <Modal
        footer={false}
        title="Đăng Ký"
        visible={this.state.visibleRegister}
        onCancel={this.closeRegister}
      >
        <WrappedRegistrationForm />
      </Modal>
    );
  };
  renderSpecialCustom = () => {
    const check: any = localStorage.getItem("key");
    const info: any = JSON.parse(check);
    if (check && info.login.role === "1") {
      return (
        <Menu.Item onClick={this.showRegisterForm} key="9">
          <Icon type="plus-circle" />
          <span>Tạo Tài Khoản Mới</span>
        </Menu.Item>
      );
    } else {
      return;
    }
  };
  infoUser = () => {
    if (localStorage.getItem("key")) {
      const key: any = localStorage.getItem("key");
      const info: any = JSON.parse(key);
      return info.login.fullName;
    } else {
      return "Tài Khoản";
    }
  };
  renderIconLogin = () => {
    if (!localStorage.getItem("key")) {
      return (
        <Menu.Item onClick={this.showLoginForm} key="1">
          <Icon type="login" />
          <span>Đăng Nhập</span>
        </Menu.Item>
      );
    } else {
      return (
        <SubMenu
          title={
            <span>
              <Icon type="user" />
              <span>{this.infoUser()}</span>
            </span>
          }
        >
          <Menu.Item>Thông Tin Cá Nhân</Menu.Item>
          <Menu.Item>Cài Đặt</Menu.Item>
          <Menu.Item>
            <Popconfirm
              title="Bạn có chắc muốn đăng xuất chứ ？"
              onConfirm={this.logoutFunction}
              okText="Có"
              cancelText="Không"
            >
              <div>Đăng Xuất</div>
            </Popconfirm>
          </Menu.Item>
        </SubMenu>
      );
    }
  };
  modal = () => {
    const { visible } = this.state;
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Ảnh Sản Phẩm</div>
      </div>
    );
    const { imageUrl } = this.state;
    return (
      <Modal
        onCancel={this.handleCancel}
        title="Tạo Mới"
        footer={false}
        visible={visible}
      >
        <Form layout="vertical">
          <Form.Item>
            <Input
              required
              onChange={event => this.isChanged(event)}
              prefix={<Icon type="form" style={{ color: "rgba(0,0,0,.25)" }} />}
              name="title"
              placeholder="Tên Sản Phẩm"
            />
          </Form.Item>
          <Form.Item>
            <Input
              required
              onChange={event => this.isChanged(event)}
              prefix={
                <Icon type="filter" style={{ color: "rgba(0,0,0,.25)" }} />
              }
              name="quantity"
              type="number"
              placeholder="Số Lượng Sản Phẩm"
            />
          </Form.Item>
          <Form.Item>
            <TextArea
              required
              onChange={event => this.isChanged(event)}
              placeholder="Giới Thiệu Sản Phẩm"
              rows={4}
              cols={60}
              name="content"
            />
          </Form.Item>
          <Form.Item>
            <Input
              onChange={event => this.isChanged(event)}
              prefix={
                <Icon
                  type="account-book"
                  style={{ color: "rgba(0,0,0,.25)" }}
                />
              }
              type="number"
              name="price"
              placeholder="Giá Sản Phẩm"
            />
          </Form.Item>
          <Form.Item>
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
              ) : (
                uploadButton
              )}
            </Upload>
          </Form.Item>
          <Button onClick={() => this.handleCancel()} type="danger" ghost>
            Cancel
          </Button>
          <Button
            onClick={this.submitForm}
            htmlType="submit"
            className="ml-2"
            type="primary"
            ghost
          >
            OK
          </Button>
        </Form>
      </Modal>
    );
  };
  render() {
    const { collapsed } = this.state;
    return (
      <Fragment>
        {this.renderRegisterForm()}
        {this.modal()}
        {this.modalLogin()}
        <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu theme="dark" mode="inline">
            {this.renderIconLogin()}
            <Menu.Item key="3">
              <Icon type="pie-chart" />
              <span>Xem Tất Cả Sản Phẩm</span>
            </Menu.Item>
            <Menu.Item key="4" onClick={this.showModal}>
              <Icon type="diff" />
              <span>Thêm Sản Phẩm Mới</span>
            </Menu.Item>
            <SubMenu
              key="5"
              title={
                <span>
                  <Icon type="team" />
                  <span>Team</span>
                </span>
              }
            >
              <Menu.Item>Team 1</Menu.Item>
              <Menu.Item>Team 2</Menu.Item>
            </SubMenu>
            {this.renderSpecialCustom()}
          </Menu>
        </Sider>
      </Fragment>
    );
  }
}
export default Side;
