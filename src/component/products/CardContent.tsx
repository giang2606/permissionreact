import * as React from "react";
import { Fragment } from "react";
import { Col } from "antd";
import Meta from "antd/lib/card/Meta";
export interface Props {
  title: string;
  content: string;
  quantity: string;
  price: string;
}

export default class CardContent extends React.Component<Props> {
  public render() {
    return (
      <Fragment>
        <Meta
          className="mb-2 text-wrap font-content"
          title={this.props.title}
          description={this.props.content}
        />
        <Col className="text-wrap font-content" span={12}>
          Số lượng: {this.props.quantity}
        </Col>
        <Col className="text-wrap font-content" span={12}>
          Giá tiền: {parseInt(this.props.price).toLocaleString()} đ
        </Col>
      </Fragment>
    );
  }
}
