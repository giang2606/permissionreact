import * as React from "react";
import {
  Icon,
  Input,
  Button,
  Card,
  Popconfirm,
  Form,
  Col,
  Upload,
  message,
  Badge
} from "antd";
import { getProductQuery } from "../../queries/queries";
import CardContent from "./CardContent";
const { TextArea } = Input;
export interface Props {
  id: string;
  title: string;
  content: string;
  price: string;
  quantity: string;
  deleteProductMutation: any;
  updateProductMutation: any;
  img: String;
  userName: String;
}
export interface State {
  editStatus: boolean;
  quantity: string;
  title: string;
  content: string;
  price: string;
  loading: boolean;
  imageUrl: any;
  UserID: string;
}
let getBase64 = (img: any, callback: any) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};
let beforeUpload = (file: any) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
};
export default class ListProducts extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      editStatus: true,
      quantity: this.props.quantity,
      title: this.props.title,
      content: this.props.content,
      price: this.props.price,
      loading: false,
      imageUrl: this.props.img,
      UserID: ""
    };
  }
  handleChange = (info: any) => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        this.setState({
          imageUrl: imageUrl,
          loading: false
        })
      );
    }
  };
  editProduct = () => {
    if (!localStorage.getItem("key")) {
      message.warning("Vui lòng đăng nhập để sử dụng chức năng này!");
      return;
    } else {
      this.setState({
        editStatus: !this.state.editStatus
      });
    }
  };
  submitForm = (event: any) => {
    this.setState({ editStatus: !this.state.editStatus });
    event.preventDefault();
    // let id: any = localStorage.getItem("key");
    // let idUser: any = JSON.parse(id);
    this.props.updateProductMutation({
      variables: {
        id: this.props.id,
        quantity: this.state.quantity,
        title: this.state.title,
        content: this.state.content,
        price: this.state.price,
        img: this.state.imageUrl,
        // UserID: idUser.login.id,
        userName: this.props.userName
      },
      refetchQueries: [{ query: getProductQuery }]
    });
  };
  editDescript = () => {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Ảnh Sản Phẩm</div>
      </div>
    );
    const { imageUrl } = this.state;
    if (this.state.editStatus === false) {
      return (
        <Form onSubmit={this.submitForm} layout="inline">
          <Form.Item>
            <Input
              onChange={event => this.isChanged(event)}
              name="title"
              placeholder="Tên Sản Phẩm"
              defaultValue={this.props.title}
            />
          </Form.Item>
          <Form.Item>
            <TextArea
              onChange={event => this.isChanged(event)}
              defaultValue={this.props.content}
              placeholder="Giới Thiệu Sản Phẩm"
              rows={1}
              cols={60}
              name="content"
            />
          </Form.Item>
          <Form.Item>
            <Input
              onChange={event => this.isChanged(event)}
              name="price"
              defaultValue={this.props.price}
              placeholder="Giá Sản Phẩm"
            />
            <Input
              onChange={event => this.isChanged(event)}
              name="quantity"
              defaultValue={this.props.quantity}
              placeholder="Số Lượng Sản Phẩm"
            />
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? (
                <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
              ) : (
                uploadButton
              )}
            </Upload>
            <Button htmlType="submit" type="link" icon="check" size="large" />
            <Button
              style={{ color: "red" }}
              onClick={this.editProduct}
              type="link"
              icon="close"
              size="large"
              className="ml-1"
            />
          </Form.Item>
        </Form>
      );
    } else {
      return (
        <CardContent
          title={this.props.title}
          content={this.props.content}
          quantity={this.props.quantity}
          price={this.props.price}
        />
      );
    }
  };

  deleteProduct = () => {
    if (!localStorage.getItem("key")) {
      message.warning("Vui lòng đăng nhập để sử dụng chức năng này!");
      return;
    } else {
      this.props.deleteProductMutation({
        variables: {
          id: this.props.id
        },
        refetchQueries: [{ query: getProductQuery }]
      });
      message.error("Xoá thành công!");
    }
  };
  isChanged = (event: any) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      ...this.state,
      [name]: value
    });
  };
  renderPicture = () => {
    if (this.state.imageUrl === "") {
      return "https://congngheads.com/media/images/anh-dep/hinh-nen-thien-nhien-dep-cho-laptop-1556704186/bo-hinh-nen-may-tinh-gai-xinh-dep-mac-ao-vao-de-thuong-kute-full-hd-4.jpg";
    } else {
      return this.state.imageUrl;
    }
  };
  render() {
    return (
      <Col lg={6} md={8} xs={24} sm={4} className="mb-2">
        <Card
          style={{ width: "90%" }}
          cover={
            <img
              className="img-g"
              height="190px"
              alt="example"
              src={this.renderPicture()}
            />
          }
          actions={[
            <Popconfirm
              onConfirm={this.deleteProduct}
              title="Bạn có chắc muốn xoá ？"
              icon={<Icon type="question-circle-o" style={{ color: "red" }} />}
            >
              <Icon
                onClick={this.editDescript}
                style={{ color: "red" }}
                type="delete"
                key="delete"
              />
            </Popconfirm>,
            <Icon onClick={this.editProduct} type="edit" key="edit" />
          ]}
        >
          <Badge className="badge-g" count={this.props.userName}></Badge>
          {this.editDescript()}
        </Card>
      </Col>
    );
  }
}
