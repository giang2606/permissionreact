import * as React from "react";
import "./App.css";
import { Layout, Row } from "antd";
import Side from "./component/Layout/Layout";
import ListProducts from "./component/products/ListProduct";
import { graphql } from "react-apollo";
import {
  getProductQuery,
  creatProductMutation,
  deleteProductMutation,
  updateProductMutation,
  loginQuery
} from "./queries/queries";
import flowright from "lodash.flowright";
const { Content } = Layout;
export interface Props {
  creatProductMutation: any;
  getProductQuery: any;
  deleteProductMutation: any;
  updateProductMutation: any;
  createUserMutation: any;
  loginQuery: any;
  data: any;
  img: String;
  history: any;
}
export interface State {
  data: any;
}
class App extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: []
    };
  }
  renderAll = (): any => {
    let data = this.props.getProductQuery;
    if (data.loading) {
      return "Loading....";
    } else {
      return data.products.map((item: any, index: any) => {
        return (
          <ListProducts
            userName={item.userName}
            updateProductMutation={this.props.updateProductMutation}
            price={item.price}
            quantity={item.quantity}
            img={item.img}
            key={index}
            id={item.id}
            content={item.content}
            title={item.title}
            deleteProductMutation={this.props.deleteProductMutation}
          />
        );
      });
    }
  };
  renderSingle = (): void => {
    const check: any = localStorage.getItem("key");
    const info: any = JSON.parse(check);
    let data = this.props.getProductQuery.products;
    return data.map((item: any, index: any) => {
      if (item.UserID === info.login.id) {
        return (
          <ListProducts
            userName={item.userName}
            updateProductMutation={this.props.updateProductMutation}
            price={item.price}
            quantity={item.quantity}
            img={item.img}
            key={index}
            id={item.id}
            content={item.content}
            title={item.title}
            deleteProductMutation={this.props.deleteProductMutation}
          />
        );
      } else {
        return false;
      }
    });
  };
  renderProduct = () => {
    let key: any = localStorage.getItem("key");
    let data: any = JSON.parse(key);
    if (localStorage.getItem("key") && data.login.role !== "1") {
      return this.renderSingle();
    } else {
      return this.renderAll();
    }
  };
  public render() {
    if (this.props.getProductQuery.loading === false) {
      return (
        <Layout className="App" style={{ minHeight: "100vh" }}>
          <Side
            history={this.props.history}
            loginQuery={this.props.loginQuery}
            creatProductMutation={this.props.creatProductMutation}
          />
          <Layout>
            <Content style={{ margin: "0 16px" }}>
              <Row className="mt-1 p-1">{this.renderProduct()}</Row>
            </Content>
          </Layout>
        </Layout>
      );
    } else {
      return "loading....";
    }
  }
}
export default flowright(
  graphql(getProductQuery, { name: "getProductQuery" }),
  graphql(creatProductMutation, { name: "creatProductMutation" }),
  graphql(deleteProductMutation, { name: "deleteProductMutation" }),
  graphql(updateProductMutation, { name: "updateProductMutation" }),
  graphql(loginQuery, { name: "loginQuery" })
)(App);
