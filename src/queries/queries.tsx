import { gql } from "apollo-boost";
const getProductQuery = gql`
  {
    products {
      UserID
      id
      title
      quantity
      content
      price
      img
      userName
    }
  }
`;
const creatProductMutation = gql`
  mutation(
    $title: String!
    $content: String!
    $quantity: String!
    $price: String!
    $img: String!
    $UserID: String!
    $userName: String!
  ) {
    createProduct(
      input: {
        title: $title
        content: $content
        quantity: $quantity
        price: $price
        img: $img
        UserID: $UserID
        userName: $userName
      }
    ) {
      id
      title
      quantity
      content
      price
      img
      UserID
      userName
    }
  }
`;
const deleteProductMutation = gql`
  mutation($id: String!) {
    delete(id: $id) {
      title
      content
    }
  }
`;
const updateProductMutation = gql`
  mutation(
    $id: String!
    $title: String!
    $content: String!
    $quantity: String!
    $price: String!
    $img: String!
    $userName: String!
  ) {
    updateProduct(
      id: $id
      product: {
        title: $title
        content: $content
        quantity: $quantity
        price: $price
        img: $img
        userName: $userName
      }
    ) {
      title
      content
    }
  }
`;
const loginQuery = gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      fullName
      role
    }
  }
`;
const getProductWithUser = gql`
  mutation($id: String!) {
    user(id: $id) {
      products {
        id
        title
        quantity
        price
        img
        content
        UserID
        userName
      }
    }
  }
`;
const createUserMutation = gql`
  mutation(
    $fullName: String!
    $password: String!
    $email: String!
    $role: String!
  ) {
    createUser(
      input: {
        fullName: $fullName
        password: $password
        email: $email
        role: $role
      }
    ) {
      email
    }
  }
`;
export {
  getProductWithUser,
  getProductQuery,
  creatProductMutation,
  deleteProductMutation,
  updateProductMutation,
  loginQuery,
  createUserMutation
};
